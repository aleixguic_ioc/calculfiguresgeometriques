package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author aleix
 * @version 2.0
 */
public class Quadrat implements FiguraGeometrica{
    private final double costat;
    
    public Quadrat() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud del costat del quadrat: ");
        this.costat = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return costat * costat;
    }
    
    @Override
    public double calcularPerimetre() {
        return 4 * costat;
    }
}
