package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author aleix
 * @version 2.1
 */
public class Triangle implements FiguraGeometrica {
    private final double costat1;
    private final double costat2;
    private final double costat3;

    public Triangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud del primer costat del triangle: ");
        this.costat1 = scanner.nextDouble();
        System.out.println("Introduïu la longitud del segon costat del triangle: ");
        this.costat2 = scanner.nextDouble();
        System.out.println("Introduïu la longitud del tercer costat del triangle: ");
        this.costat3 = scanner.nextDouble();
    }

    @Override
    public double calcularArea() {
        // Utilitzant la fórmula de Heró d'Àrea per a un triangle
        double semiPerimetre = calcularPerimetre() / 2;
        return Math.sqrt(semiPerimetre * (semiPerimetre - costat1) * (semiPerimetre - costat2) * (semiPerimetre - costat3));
    }

    @Override
    public double calcularPerimetre() {
        return costat1 + costat2 + costat3;
    }
}
